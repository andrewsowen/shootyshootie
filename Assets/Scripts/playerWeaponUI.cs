﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class playerWeaponUI : MonoBehaviour  {
	private GameObject player;
	Text text;
	private int equippedWeapon;
	private float [,] weaponArray;
	private int[] ammoArray;
	private int ammoRemaining;
	private int ammoInventory;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		player = GameObject.FindGameObjectWithTag ("Player");
		weaponArray = player.GetComponent<playerScript> ().weapons;
		ammoArray = player.GetComponent<playerScript> ().ammo;
	}

	// Update is called once per frame
	void Update () {
		equippedWeapon = player.GetComponent<playerScript> ().equippedWeapon;
		ammoRemaining = System.Convert.ToInt32 (weaponArray [equippedWeapon - 1, 2]);
		ammoInventory = ammoArray [System.Convert.ToInt32 (weaponArray [equippedWeapon - 1, 6]) - 1];
		text.text = "Ammo: " + ammoRemaining + " / " + ammoInventory;
	}
}
