﻿using UnityEngine;
using System.Collections;

public class bloodSplatScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//randomize rotation
		float rotation = Random.Range (0, 359);
		Quaternion rot = Quaternion.Euler (0, 0, rotation);
		transform.rotation = rot;

		//rondomize size
		float size = Random.Range (.03f, .08f);
		transform.localScale = new Vector3 (size, size, 1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
