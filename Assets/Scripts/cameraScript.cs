﻿using UnityEngine;
using System.Collections;

public class cameraScript : MonoBehaviour {
	private GameObject player;
	Camera cam;
	public float speed;
	private float xCord;
	private float yCord;

	// Use this for initialization
	void Start () {
		var objects = GameObject.FindGameObjectsWithTag ("Player");
		if(objects != null && objects.Length > 0)
		{
			player = objects[0];
		}
		cam = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 viewPos = cam.WorldToViewportPoint (player.transform.position);
		var cameraMove = new Vector3(xCord, yCord, 0);
		if (viewPos.x >= 0.75F) {
			xCord = 1;
		} else if (viewPos.x <= 0.25F) {
			xCord = -1;
		} else {
			xCord = 0;
		}
		if (viewPos.y >= 0.75F) {
			yCord = 1;
		} else if (viewPos.y <= 0.25F) {
			yCord = -1;
		} else {
			yCord = 0;
		}
		cam.transform.position += cameraMove * speed * Time.deltaTime;	
	}
}
