﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class uiScript : MonoBehaviour {
	float flashTime = .15f;
	public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (player.GetComponent<playerScript>().playerHit) {
			gameObject.GetComponent<Image> ().enabled = true;
			if (flashTime <= 0) {
				flashTime = .15f;
				player.GetComponent<playerScript>().playerHit = false;
				gameObject.GetComponent<Image> ().enabled = false;
			}
			flashTime -= Time.deltaTime;
		}
	}
}
