﻿using UnityEngine;
using System.Collections;

public class healthBarScript : MonoBehaviour {
	private float healthPercent;
	public GameObject parent;
	private float startingHealth;
	public GameObject ammoDrop;
    public GameObject healthDrop;
    public GameObject itemDrop;

	// Use this for initialization
	void Start () {
		startingHealth = parent.GetComponent<enemyScript> ().health;
	}
	
	// Update is called once per frame
	void Update () {
		healthPercent = parent.GetComponent<enemyScript> ().health / startingHealth;
		transform.localScale = new Vector3 (.4f * healthPercent, .5f, 1);

		//remove enemy object if health is at or below zero
		if (parent.GetComponent<enemyScript> ().health <= 0 && !parent.GetComponent<enemyScript>().isDead) {
            //give player xp equal to enemies xp value
            GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[1] += parent.GetComponent<enemyScript>().xp;
            //check if that levels up the player
            if(GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[1]>= ((4+GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[0])* GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[0]))
            {
                //subtract xp required for level up from xp collected
                GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[1] -= ((4 + GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[0]) * GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[0]);
                //increase the players level by one
                GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerStats[0]++;
                //increase players available skill points by 1 
                GameObject.FindGameObjectWithTag("Player").GetComponent<playerScript>().playerSkillLevel[5]++;
            }

            int randA = Random.Range (1, 7);
			int randB = Random.Range (1, 7);
			if (randA >= randB) {
                int rand = Random.Range(1, 7);
                if (rand > 0 && rand <= 3)
                {
                    GameObject.Instantiate(ammoDrop, parent.transform.position, transform.rotation);
                }
                else if (rand > 3 && rand <= 5)
                {

                }
                else
                {
                    GameObject.Instantiate(healthDrop, parent.transform.position, transform.rotation);                    
                }
			}

            //call death function
            parent.GetComponent<enemyScript>().killObject();

        }
	}
}
