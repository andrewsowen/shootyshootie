﻿using UnityEngine;
using System.Collections;

public class ammoCrateScript : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
			int randAmmoType = Random.Range (0, 5);
			int randAmmoAmount = Random.Range (5, 15);
			col.GetComponent<playerScript> ().ammo [randAmmoType] += randAmmoAmount;
			Destroy (gameObject);
		}
	}
}
