﻿using UnityEngine;
using System.Collections;

public class enemyBulletBehavior : MonoBehaviour {
	public int damage;
	public GameObject bloodSpray;
	public GameObject bloodSplat;

	void OnBecameInvisible()
	{
		Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "enemy" || col.tag == "bullet" || col.tag == "colTrigger") {
		} else if (col.tag == "crate" && !col.GetComponent<sceneObjects> ().providingCover) {
		} else if (col.tag == "Player") {
			//calculate a rotation based on bullet and create a blood spray effect in the reverse direction
			Vector2 bulletPosition = col.transform.position;
			bulletPosition.x = bulletPosition.x - transform.position.x;
			bulletPosition.y = bulletPosition.y - transform.position.y;
			float angle = Mathf.Atan2 (bulletPosition.y, bulletPosition.x) * Mathf.Rad2Deg;	
			var rotationVector = transform.rotation.eulerAngles;
			rotationVector.x = angle;
			rotationVector.y = 90;
			rotationVector.z = 0;
			GameObject.Instantiate (bloodSpray, transform.position, Quaternion.Euler (rotationVector));

			//add blood splat to ground
			GameObject.Instantiate (bloodSplat, col.transform.position, col.transform.rotation);

			//flash hit UI
			col.GetComponent<playerScript> ().playerHit = true;

			col.GetComponent<playerScript> ().health -= damage;
			Destroy (gameObject);
		} else {
			Destroy (gameObject);
		}
	}
}
