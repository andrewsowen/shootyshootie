﻿using UnityEngine;
using System.Collections;

public class basePlayer : MonoBehaviour
{

    public int[] playerSkillLevels;
    

    void Awake()
    {
        // perserve this object between scenes
        Object.DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        // Use this for initialization
        playerSkillLevels = new int[] { 1, 1, 1, 1, 1, 1 };
    }

}
