﻿using UnityEngine;
using System.Collections;

public class bulletBehavior : MonoBehaviour {
	public int damage =1;
	void OnBecameInvisible()
	{
		Destroy (gameObject);
	}
		
	void OnTriggerEnter2D(Collider2D col)
	{		
		if (col.tag == "Player" || col.tag == "enemyBullet" || col.tag == "colTrigger") {
		} else if (col.tag == "crate" && !col.GetComponent<sceneObjects> ().providingCover) {
		} else {
			Destroy (gameObject);
		}
	}
}
