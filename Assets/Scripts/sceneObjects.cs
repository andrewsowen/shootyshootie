﻿using UnityEngine;
using System.Collections;

public class sceneObjects : MonoBehaviour 
{
	public int health;
	public bool destructable;
	public bool providingCover;
	private int damage;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (health <= 0) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "bullet") {
			if (destructable && providingCover) {
				damage = col.GetComponent<bulletBehavior>().damage;
				health -= damage;
			}
		}
		if (col.tag == "enemyBullet") {			
			if (destructable && providingCover) {
				damage = col.GetComponent<enemyBulletBehavior>().damage;
				health -= damage;
			}
		}
	}
}
