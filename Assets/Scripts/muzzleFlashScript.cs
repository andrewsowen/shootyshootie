﻿using UnityEngine;
using System.Collections;

public class muzzleFlashScript : MonoBehaviour {
	float flashTime = .05f;
	public bool shotFired;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (shotFired) {
			gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			if (flashTime <= 0) {
				flashTime = .05f;
				shotFired = false;
				gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			}
			flashTime -= Time.deltaTime;
		}
	}
}
