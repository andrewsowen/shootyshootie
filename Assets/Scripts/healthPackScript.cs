﻿using UnityEngine;
using System.Collections;

public class healthPackScript : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            var currentHealth = col.GetComponent<playerScript>().health;
            var maxHeatlth = col.GetComponent<playerScript>().skillTable[2, col.GetComponent<playerScript>().playerSkillLevel[2] - 1];
            //randomly heal between 2 and 5 health
            var rand = Random.Range(2, 5);
            //make sure it does not push the player past max health
            if (rand + currentHealth > maxHeatlth)
            {
                col.GetComponent<playerScript>().health = maxHeatlth;
            }
            else
            {
                col.GetComponent<playerScript>().health += rand;               
            }
            Destroy(gameObject);
        }
       
    }
}
