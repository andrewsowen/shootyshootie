﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class playerHealthUI : MonoBehaviour {
	private GameObject player;
	Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "Health: " + (player.GetComponent<playerScript> ().health);
	}
}
