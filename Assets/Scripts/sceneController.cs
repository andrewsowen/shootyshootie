﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class sceneController : MonoBehaviour {
    public GameObject trigger;
    public GameObject levelPt2;
    public GameObject totalBox;
    public GameObject accBox;
    public GameObject whBox;
    public GameObject speedBox;
    public GameObject stmBox;
    public GameObject stghBox;
    public GameObject accPlus;
    public GameObject accMinus;
    public GameObject whPlus;
    public GameObject whMinus;
    public GameObject speedPlus;
    public GameObject speedMinus;
    public GameObject stmPlus;
    public GameObject stmMinus;
    public GameObject stghPlus;
    public GameObject stghMinus;
    public GameObject player;
    public GameObject characterMenu;
    public bool menuOpen;
    private int[] playerSkillLevels;
    private int[] startSkillLevels;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	if (trigger.GetComponent<enemyScript>().isDead)
        {
            Destroy(GameObject.FindGameObjectWithTag("door"));
            levelPt2.SetActive(true);           
        }
	}
    
    public void openCharacterMenu()
    {
        menuOpen = true;
        characterMenu.SetActive(true);
        startSkillLevels = player.GetComponent<playerScript>().playerSkillLevel;
        playerSkillLevels = player.GetComponent<playerScript>().playerSkillLevel;

        //fill in boxes with data from array
        accBox.GetComponent<InputField>().text = playerSkillLevels[0].ToString();
        whBox.GetComponent<InputField>().text = playerSkillLevels[1].ToString();
        speedBox.GetComponent<InputField>().text = playerSkillLevels[3].ToString();
        stmBox.GetComponent<InputField>().text = playerSkillLevels[2].ToString();
        stghBox.GetComponent<InputField>().text = playerSkillLevels[4].ToString();
        totalBox.GetComponent<InputField>().text = playerSkillLevels[5].ToString();

        //disable minus buttons to start off
        accMinus.GetComponent<Button>().interactable = false;
        whMinus.GetComponent<Button>().interactable = false;
        speedMinus.GetComponent<Button>().interactable = false;
        stmMinus.GetComponent<Button>().interactable = false;
        stghMinus.GetComponent<Button>().interactable = false;

        //check if there is an available skill point otherwise disable plus buttons
        if (int.Parse(totalBox.GetComponent<InputField>().text) == 0)
        {
            accPlus.GetComponent<Button>().interactable = false;
            whPlus.GetComponent<Button>().interactable = false;
            speedPlus.GetComponent<Button>().interactable = false;
            stmPlus.GetComponent<Button>().interactable = false;
            stghPlus.GetComponent<Button>().interactable = false;
        }
    }

    public void closeCharacterMenu()
    {
        menuOpen = false;

        //write any changes back to the player array
        player.GetComponent<playerScript>().playerSkillLevel = playerSkillLevels;

        //hide menu
        characterMenu.SetActive(false);
    }

    public void changeSkill(string skillButton)
    {
        switch (skillButton)
        {
            case "accuracy+":
                playerSkillLevels[0]++;
                playerSkillLevels[5]--;
                accMinus.GetComponent<Button>().interactable = true;
                updateSkillScreen();
                break;
            case "accuracy-":
                playerSkillLevels[0]--;
                playerSkillLevels[5]++;
                if (playerSkillLevels[0] == startSkillLevels[0])
                {
                    accMinus.GetComponent<Button>().interactable = false;
                }
                updateSkillScreen();
                break;
            case "weapon handling+":
                playerSkillLevels[1]++;
                playerSkillLevels[5]--;
                whMinus.GetComponent<Button>().interactable = true;
                updateSkillScreen();
                break;
            case "weapon handling-":
                playerSkillLevels[1]--;
                playerSkillLevels[5]++;
                if (playerSkillLevels[1] == startSkillLevels[1])
                {
                    whMinus.GetComponent<Button>().interactable = false;
                }
                updateSkillScreen();
                break;
            case "speed+":
                playerSkillLevels[3]++;
                playerSkillLevels[5]--;
                speedMinus.GetComponent<Button>().interactable = true;
                updateSkillScreen();
                break;
            case "speed-":
                playerSkillLevels[3]--;
                playerSkillLevels[5]++;
                if (playerSkillLevels[3] == startSkillLevels[3])
                {
                    speedMinus.GetComponent<Button>().interactable = false;
                }
                updateSkillScreen();
                break;
            case "stamina+":
                playerSkillLevels[2]++;
                playerSkillLevels[5]--;
                stmMinus.GetComponent<Button>().interactable = true;
                updateSkillScreen();
                break;
            case "stamina-":
                playerSkillLevels[2]--;
                playerSkillLevels[5]++;
                if (playerSkillLevels[2] == startSkillLevels[2])
                {
                    stmMinus.GetComponent<Button>().interactable = false;
                }
                updateSkillScreen();
                break;
            case "strength+":
                playerSkillLevels[4]++;
                playerSkillLevels[5]--;
                stghMinus.GetComponent<Button>().interactable = true;
                updateSkillScreen();
                break;
            case "strength-":
                playerSkillLevels[4]--;
                playerSkillLevels[5]++;
                if (playerSkillLevels[4] == startSkillLevels[4])
                {
                    stghMinus.GetComponent<Button>().interactable = false;
                }
                updateSkillScreen();
                break;
            default:
                break;
        }
    }

    void updateSkillScreen()
    {
        //set text boxes to player's skill value
        accBox.GetComponent<InputField>().text = playerSkillLevels[0].ToString();
        whBox.GetComponent<InputField>().text = playerSkillLevels[1].ToString();
        speedBox.GetComponent<InputField>().text = playerSkillLevels[3].ToString();
        stmBox.GetComponent<InputField>().text = playerSkillLevels[2].ToString();
        stghBox.GetComponent<InputField>().text = playerSkillLevels[4].ToString();
        totalBox.GetComponent<InputField>().text = playerSkillLevels[5].ToString();

        //check if there is an available skill point otherwise disable plus buttons
        if (int.Parse(totalBox.GetComponent<InputField>().text) == 0)
        {
            accPlus.GetComponent<Button>().interactable = false;
            whPlus.GetComponent<Button>().interactable = false;
            speedPlus.GetComponent<Button>().interactable = false;
            stmPlus.GetComponent<Button>().interactable = false;
            stghPlus.GetComponent<Button>().interactable = false;
        }
        else
        {
            accPlus.GetComponent<Button>().interactable = true;
            whPlus.GetComponent<Button>().interactable = true;
            speedPlus.GetComponent<Button>().interactable = true;
            stmPlus.GetComponent<Button>().interactable = true;
            stghPlus.GetComponent<Button>().interactable = true;
        }
    }
}
